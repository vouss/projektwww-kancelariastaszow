$(document).ready(function(){
	setInterval("checkAnchor()", 150);
});
var currentAnchor = null;

function checkAnchor(){
	if(currentAnchor != document.location.hash){
		currentAnchor = document.location.hash;
		
		if(!currentAnchor) {
			query = "page=home";
			//$("#main").removeClass("menu").addClass("current_page_item");
		} else {
			//Creates the  string callback. This converts the url URL/#main&id=2 in URL/?section=main&id=2
			var splits = currentAnchor.substring(1).split('&');
			//Get the section
			var section = splits[0];
			delete splits[0];
			//Create the params string
			var params = splits.join('&');
			var query = "page=" + section + params;
		}
		//Send the petition
		$.get("callbacks.php",query, function(data){
			$("#content").html(data);
		});
	}
	
/*	$("#menu li a").click(function(e){
				$('#menu li a').removeClass("current_page_item");
				$(this).addClass("current_page_item");
				e.preventDefault();
			});*/
}