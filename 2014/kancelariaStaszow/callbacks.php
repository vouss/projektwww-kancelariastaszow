<?php
	switch($_GET['page']){
		case 'home':
		//----------------------Glowna Strona-----------------
?>
	<div id="sidebar">
			<div id="stwo-col">
				<div class="sbox1">
					<h2>KANCELARIA</h2>
					<ul class="style2" style="padding-top: 25px; text-align: justify; line-height: 25px">
						Kancelaria prowadzona jest przez Radcę Prawnego Magdalenę Gondek, absolwentkę Uniwersytetu Marii Curie - Skłodowskiej w Lublinie,
						wpisaną na listę Radców Prawnych, prowadzona przez Okręgową Izbę Radców Prawnych w Kielcach pod Nr KL - 930.
					</ul>
				</div>
				<div class="sbox2">
					<h2>USŁUGI</h2>
					<ul class="style2" style="padding-top: 25px; text-align: center">
						<li class="icon"><a>porady prawne</a></li>
						<li class="icon"><a>opinie i analizy prawne</a></li>
						<li class="icon"><a>pisma procesowe</a></li>
						<li class="icon"><a>projekty umów</a></li>	
						<li class="icon"><a>reprezentowanie przed sądami wszystkich instancji</a></li>
					</ul>
				</div>
				<div class="sbox3">
					<h2>KONTAKT</h2>
					<ul class="style2" style="padding-top: 25px; text-align: center; line-height: 25px">
						ul. Długa 6/14<br />
						28-200 Staszów<br />
						<hr width="50%"/>
						tel.<a>601 052 429</a><br />
						e-mail: magda@kancelaria-staszow.pl<br />
					</ul>
				</div>
			</div>
		</div>
<?php
//----------------------Glowna Strona | koniec -----------------
		break;
	case 'uslugi':
	//----------------------USLUGI-----------------	
?>
	<div id="sidebar">
		<h2>Zakres świadczonych usług prawnych obejmuje:</h2><br />
		<div id="stwo-col">
			<div class="sbox1" style="width: 48%">
				<ul class="style2">
							<li class="icon"><a>prawo cywilne materialne i procesowe</a></li>
							<li class="icon" style="padding-top: 105px"><a>prawo gospodarcze</a></li>
							<li class="icon" style="padding-top: 180px"><a>prawo pracy i ubezpieczeń społecznych</a></li>
							<li class="icon" style="padding-top: 120px"><a>prawo administracyjne materialne i procesowe</a></li>
							<li class="icon" style="padding-top: 70px"><a>obsługa prawna innych przedsiębiorców oraz fundacji i stowarzyszeń</a></li>
						</ul>
			</div>
			<div class="sbox2" style="width: 48%; text-align: justify; line-height: 25px">
				<ul class="style2">
					<li style="color: #454445; border: none">Doradztwo w zakresie prawa zobowiązań, prawa rodzinnego, prawa rzeczowego, prawa spadkowego,
					prowadzenie spraw o zapłatę, ochronę dóbr osobistych, o zniesienie współwłasności,
						spraw zasiedzeniowych<br />i windykacyjnych</li>
						<li style="color: #454445; border: none">Pełna obsługa korporacyjna spółek obejmująca pomoc przy zakładaniu<br />i rejestracji, aktualizację danych w rejestrach,
					przygotowywanie uchwał organów spółek, obsługę prawną organów spółek, udzielanie opinii prawnych w zakresie bieżącej działalności firmy,
							uczestnictwo w negocjacjach z kontrahentami, pomoc prawna w procedurze przetargowej, dokonywanie czynności związanych z zakończeniem działalności spółki,
							w tym prowadzenie postępowania likwidacyjnego, upadłościowego i układowego</li>
					<li style="color: #454445; border: none">Wydawanie opinii z zakresu prawa pracy, przygotowywanie umów o pracę, kontraktów menadżerskich, 
					umów o zakazie konkurencji, regulaminów pracy, regulaminów wynagradzania, prowadzenie sporów sądowych z zakresu prawa pracy,
					doradztwo w zakresie zwolnień grupowych, reprezentowanie w dochodzeniu roszczeń przed organami ubezpieczeń społecznych</li>
					<li style="color: #454445; border: none">
					Prawo samorządu terytorialnego, prawo budowlane, prawo ochrony środowiska, prawo zagospodarowania przestrzennego, prawo zamówień publicznych.</li>
				</ul>
			</div>
		</div>
	</div>

<?php
//----------------------USLUGI | koniec -----------------	
		break;
	case 'kontakt':
	//----------------------KONTAKT -----------------		
?>
	<div id="sidebar" style="padding-bottom: 50px">
		<div id="stwo-col">
			<div class="sbox1" style="width: 48%">
				<h2>KONTAKT</h2>
				<ul class="style2" style="text-align: center; line-height: 25px">
					ul. Długa 6/14<br />
					28-200 Staszów<br />
					<hr width="50%"/>
					tel.<a>601 052 429</a><br />
					e-mail: magda@kancelaria-staszow.pl<br />
				</ul>
			</div>
			<div class="sbox2" style="width: 48%">
				<h2>GODZINY OTWARCIA</h2>
				<ul class="style2" style="text-align: center; line-height: 25px">
					Poniedziałek 9.00 – 16.00<br />
					Piątek 9.00 – 16.00<br />
					<a>Po wcześniejszym uzgodnieniu telefonicznym istnieje możliwość spotkania z Klientem w innym, dogodnym dla niego terminie</a>
				</ul>
			</div>
	</div>
	</div>
	<div id="sidebar" style="padding-bottom: 50px">
		<div id="stwo-col">
			
			<div class="sbox2" style="width: 48%">
				<h2>Mapa</h2>
				<ul class="style2" style="text-align: center; line-height: 25px">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2534.333050380689!2d21.166424499999998!3d50.565171899999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473d580b9821d36b%3A0x36f0566ea685f9c!2zRMWCdWdhIDY!5e0!3m2!1spl!2spl!4v1394118291561" width="450" height="337" frameborder="0" style="border:0"></iframe>
				</ul>
			</div>
	</div>
	</div>

		
<?php
//----------------------KONTAKT | koniec -----------------	
		break;
	default:
?>
	<h1>Brak podstrony!</h1>
<?php
	}
?>